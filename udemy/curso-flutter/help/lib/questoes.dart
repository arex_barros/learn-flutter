import 'package:flutter/cupertino.dart';

import 'questao.dart';
import 'resposta.dart';

class Questoes extends StatelessWidget {
  final List<Map<String, Object>> perguntas;
  final int index;
  final void Function(int) action;

  Questoes({
    @required this.perguntas,
    @required this.index,
    @required this.action,
  });

  bool get temPerguntaSelecionada {
    return index < perguntas.length;
  }

  @override
  Widget build(BuildContext context) {
    List<Map<String, Object>> respostas =
        temPerguntaSelecionada ? perguntas[index]['respostas'] : null;
    return Column(
      children: <Widget>[
        Questao(perguntas[index]['texto']),
        ...respostas
            .map((resp) => Resposta(resp['texto'], () => action(resp['nota'])))
            .toList(),
      ],
    );
  }
}
