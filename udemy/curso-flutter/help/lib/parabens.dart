import 'package:flutter/material.dart';

class Parabens extends StatelessWidget {
  final int pontos;
  final void Function() quandoReiniciar;

  Parabens(this.pontos, this.quandoReiniciar);

  String get seuNivel {
    if (pontos < 8) {
      return "Parabéns!!";
    } else if (pontos < 10) {
      return "TOP!!";
    } else if (pontos < 15) {
      return "TOP DEMAIS!!";
    } else {
      return "WELSOME!!";
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Text(
            seuNivel,
            style: TextStyle(fontSize: 28),
          ),
        ),
        TextButton(
          child: Text(
            'Reiniciar?',
            style: TextStyle(fontSize: 18),
          ),
          style: TextButton.styleFrom(primary: Colors.blue),
          onPressed: quandoReiniciar,
        ),
      ],
    );
  }
}
